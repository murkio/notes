# Childhood
“Her son, who had hardly been a child, would at least now be treated like one by the criminal justice system”

“Only in detention had it occurred to him that drudge labor in an urban armpit like Annawadi might be considered freedom”

“To his family, Abdul’s physical capability had been the mattering thing. He was the workhorse, his moral judgements irrelevant.” k
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE5MzEwMjQyNzIsMzcyNjc5OTk5XX0=
-->