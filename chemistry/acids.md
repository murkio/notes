# Acid and base theory
Acids and bases are all about protons. Acids donate protons, bases receive protons. Molecules with more hydrogens act like acids, as they can give off their hydrogens. Molecules that need an extra hydrogen act like bases (eg. $OH^-$). Acids and bases fit on a continuous spectrum, with something like water neutral. The pH scale is a measure of acidity, with lower pH indicating a more acidic solution, and higher pH indicating an alkaline substance (basic). A pH of 7 is neutral. 

As acids become more alkaline/dilated, they won't go above 7, and as alkanes become more acidic/dilated, they won't go below 7. An acid / base must be add to cross seven.

The pH scale is typically 0-14, however it **can** go above and below. It is an inverse log measurement of the number of free Hydrogen ions, as the number of hydrogen atoms can be very large or very small.

$$ pH = -log_{10}H^+$$

## Nomenclature of acids
If an acid is composed of only hydrogen and one other element, the name is _hydro-_ + the stem of the other element + _-ic acid_. This is the base from which other names stem - depending on more/less oxygens, which changes prefixes/suffixes.

Oxyacids consist of hydrogen, oxygen and another element.

|Number of atoms|Acid prefix/suffix
|--|--|
|1 more than -ic| Per- / -ic
|-ic acid|-ic
|1 fewer than -ic| -ous
|2 fewer than -ic|Hypo- / -ous
|No Oxygen|Hydro- / -ic

### Examples
Molecule | Naming | Answer
|--|--|--
HCl | No oxygen. Prefix: Hydro- suffix -ic | Hydrochloric acid
HClO|Hypo- / -ous|Hypochlorous acid
HClO$_2$|-ous|Chlorous acid
HClO$_3$|-ic|Chloric acid
HClO$_4$ | Per- / -ic| Perchloric acid

## Nomenclature of bases
Named just like a polyatomic ion. Often has an OH$^-$ group.

Formula|Name
--|--|--|
NaOH | Sodium Hydroxide
Ca(OH)$_2$ | Calcium Hydroxide
NH_3 | Ammonia (no naming scheme). In aqueous form, ammonia hydroxide.
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTEyNjM2Mjc5OTksMjY0ODU2MzIwLC03Mz
c0Njg5ODQsLTE2NzEzMjcxMzIsMTIzMTgyODIwNiwyMDU4MzEx
NjczLC0yMDg4NzQ2NjEyXX0=
-->