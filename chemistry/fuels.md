# Fuels

## Types of fuels
- Renewable
	- Biogas
		- Creates through anaerobic bacterial breakdown of organic waste.
	- Bioethanol
		- Anaerobic fermentation of sugars to produce ethanol and $CO_2$
	- Biodiesel
		- Produced through transesterification of vegetable oils and animals fats through the addition of methanol.
- Non renewable
	- Coal
		- Over time, transition from peat, to brown coal, to black coal, with decreasing water content.
	- Crude oil
		- Mixture of alkanes
		- Separated by fractional distillation
	- Natural gas
		- Extracted by fracking

## Extraction processes
### Transesterification
Used in the production of biodiesel. Three methanol molecules (other alcohols can be used, producing biodiesel with esters longer than methyl ester) break off the three fatty acids from the triglyceride. This leaves a glycerol molecule and three alkane ester chains (the biodiesel). The glycerol and other unwanted material can be separated from the biodiesel in water, as the biodiesel has a unique polarity (polar/nonpolar? Check)

## Fracking
Used in the extraction of natural gas. Pressure and material is pumped through a well into the ground, releasing trapped natural gas. Ideally, all released gas travels back up the well to be captured, however potential gas leakages and damage to the water table result in environmental damage concerns.

## Fractional distillation
The process by which crude oil, which is composed of alkanes of mixed length, are separated into groups of roughly similar length. The crude oil is heated, and separation by melting point and hence density occurs. The lightest, smallest alkanes, such as methane, travel to the top, which is coolest. The heaviest, longest alkanes remain at the bottom, which is hottest.

![Fractional distillation process](https://www.epicmodularprocess.com/wp-content/uploads/2015/11/fractional-distillation-system.jpg)

# Combustion
Can either be complete or incomplete. Complete combustion occurs in excess oxygen, and typically produces carbon dioxide and water products. Incomplete combustion occurs in limited oxygen, and typically produces carbon monoxide or carbon (soot) and water.
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTk5NzkwOTUwNiwyODUwNTc5MzZdfQ==
-->