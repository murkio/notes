# Nomenclature
## Organic
1. Identify longest carbon chain.
2. All single carbon bonds, -ane, double bond, -ene, triple bond, -yne
## Inorganic 
- Cation: Name + ‘ion’ (eg. Sodium ion)
- Anion: Ide-name (eg. Chloride) - Monoatomic

|Number of atoms|Acid prefix/suffix|Anion prefix/suffix|
|--|--|--|
|1 more than -ic| Per- / -ic | Per- / -ate
|-ic acid|-ic|Per- / -ate|
|1 fewer than -ic| -ous| -ite
|2 fewer than -ic|Hypo- / -ous|Hypo- / -ite
|No Oxygen|Hydro- / -ic | -ide
-   Ends in -ate, one more oxygen than -ite version
    
-   Ends in -ite, one less oxygen than -ate version

Naming covalent molecular
-   For the first element, start with the element name
-   For the second name, use the -ide name of the second element
-   Use prefixes to show how many elements there are (mono, di, tri, tetra, penta, hexa, hepta, octa, nona, deca
-   Never use mono prefix on first element
-   Change any ‘ao’ or ‘oo’ to ‘o’ to make sense verbally
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTc1MTE3MzMzMiwxMDgzMjE2NTcyLDU0OT
c2MzI4NSwxMDYxNDQyNjgxXX0=
-->