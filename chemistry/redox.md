# Redox
 - To determine if a redox reaction has occurred, find oxidation numbers, if it has changed, a redox reaction has occurred.
 - Reductant is oxidised and acts as the reducing agent.
 - A conjugate redox pair is the pair that differ in terms of oxidation number (eg. $Fe^{3+}$ / $Fe$)

## Half equations
The only place where electrons should be shown (cancelled out in full equations)
 - Oxidisation / reduction. Don't forget to include states. Remember that ions are free/separate if marks as aqueous.

Iron rusting in copper sulfate:

$$ Fe(s) + CuSO_4(aq)  \rightarrow  FeSO4(aq) + Cu(s)$$

Oxidisation half equation: $Fe(s)  \rightarrow  Fe2+ + 2e-$
Reduction half equation: $Cu2+(aq) + 2e^-  \rightarrow  Cu(s)$

## Redox balancing involving oxygen/hydrogen
  - Balance species other than $H$ and $O$
  - Balance $O$ with $H_2O$
  - Balance H (may have resulted from introduction of water) with H+
  - Balance charges with e- (may not be equivalent to H+, consider all charges from both sides to balance)
  - Combine two half equations, multiplying to ensure that electrons cancel from either equation. Cancel/combine reactants/products.

## Galvanic cells
 - Each container is the site of one half equation.
 - The anode container sites the oxidisation reaction (electrons freed)
 - Electrons flow through conductor/circuit to cathode, where reduction reaction _uses_ electrons.
 - 'Electrolyte' - ion solution, conducts electricity
 - Salt bridge allows movement of ions between containers, which is required for the reaction to continue.

### Two types of galvanic cells
 - Solution half cell: Electrode is inert, just a conductor (such as graphite or platinum). Reaction occurs in *solution*.
 - Metal ion-metal half cell: The electrode participates in the redox reaction.

## Electrochemical series
 - In the data book, including voltages of half equations.
 - Describes the ability of the reductant (which is oxidised) to lose electrons.
 - Lower half equations are good reductants (donate $e^-$)
 - Upper half equations are good oxidants (want $e^-$)
 - Half equation on top goes in the forward direction, half equation below goes in backwards/reverse direction.
 - Oxidisation half equation at the anode
 - Reduction half equation at the cathode.

## Example galvanic cell
Copper/copper sulfate half cell and a lead/lead sulfate half cell. Don't include the spectator sulfate.

$$
Cu^{2+}(aq) + 2e^- \rightarrow Cu(s)$$
$$
Pb(s)  \rightarrow  Pb^{2+}(aq) + 2e^-
$$
Overall:
$$
Cu^{2+}(aq) + Pb(s)  \rightarrow  Cu(s) + Pb^{2+}(aq)
$$
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTkyNDE4ODA1MSwtNTQ0MzEzNzcxLC01ND
QzMTM3NzEsNzE2OTg0MzMxLDcxNjk4NDMzMV19
-->